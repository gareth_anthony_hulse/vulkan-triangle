/* © Copyright 2019
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

std::vector<const char*>
app::vulkan::_M_get_required_instance_exts ()
{
  uint32_t glfw_extension_count = 0;
  const char** glfw_extensions;
  glfw_extensions = glfwGetRequiredInstanceExtensions (&glfw_extension_count);

  std::vector<const char*> instance_extensions (glfw_extensions, glfw_extensions + glfw_extension_count);

#ifndef NDEBUG
  instance_extensions.push_back (VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
#endif // NDEBUG

  return instance_extensions;
}
