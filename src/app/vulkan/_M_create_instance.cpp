/* © Copyright 2019
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::_M_create_instance ()
{
#ifndef NDEBUG
  if (!_M_check_validation_layer_support ())
    {
      throw std::runtime_error ("Validition layers not available!");
    }
#endif // NDEBUG
  
  VkApplicationInfo app_info = {};
  app_info.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
  app_info.pApplicationName = PACKAGE_NAME;
  app_info.applicationVersion = VK_MAKE_VERSION (1, 1, 0);
  app_info.pEngineName = "N/A";
  app_info.engineVersion = VK_MAKE_VERSION (1, 0, 0);
  app_info.apiVersion = VK_API_VERSION_1_1;

  VkInstanceCreateInfo create_info = {};
  create_info.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
  create_info.pApplicationInfo = &app_info;
  auto instance_extensions = _M_get_required_instance_exts ();
  create_info.enabledExtensionCount = static_cast<uint32_t> (instance_extensions.size ());
  create_info.ppEnabledExtensionNames = instance_extensions.data ();
  
#ifndef NDEBUG
  VkDebugUtilsMessengerCreateInfoEXT debug_create_info;
  
  create_info.enabledLayerCount = static_cast<uint32_t> (_M_validation_layers.size ());
  create_info.ppEnabledLayerNames = _M_validation_layers.data ();
  _M_populate_debug_messenger_create_info (debug_create_info);
  create_info.pNext = static_cast<VkDebugUtilsMessengerCreateInfoEXT*> (&debug_create_info);
 #else
  create_info.enabledLayerCount = 0;
  create_info.pNext = nullptr;
#endif // NDEBUG
  
  if (vkCreateInstance (&create_info, nullptr, &_M_instance) != VK_SUCCESS)
    {
      throw std::runtime_error ("Failed to create instance!");
    }
}
