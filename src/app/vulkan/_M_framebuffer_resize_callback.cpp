/* © Copyright 2019
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::_M_framebuffer_resize_callback (GLFWwindow* window, int /*width*/, int /*height*/)
{
  auto app = reinterpret_cast<app::vulkan*> (glfwGetWindowUserPointer (window));
  app->_M_framebuffer_resized = true;
}
