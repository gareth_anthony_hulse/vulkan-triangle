/* © Copyright 2019
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef NDEBUG
#include "../vulkan.hpp"

bool
app::vulkan::_M_check_validation_layer_support ()
{
  uint32_t layer_count;
  vkEnumerateInstanceLayerProperties (&layer_count, nullptr);

  std::vector<VkLayerProperties> available_layers (layer_count);
  vkEnumerateInstanceLayerProperties (&layer_count, available_layers.data ());

  for (const char* layer_name : _M_validation_layers)
    {
      bool layer_found = false;

      for (const auto& layer_properties : available_layers)
	{
	  if (strcmp (layer_name, layer_properties.layerName) == 0)
	    {
	      layer_found = true;
	      break;
	    }
	}

      if (!layer_found)
	{
	  return false;
	}
    }
  return true;
}
#endif // NDEBUG
