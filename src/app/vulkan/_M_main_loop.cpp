/* © Copyright 2019
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::_M_main_loop ()
{
  while (!glfwWindowShouldClose (_M_window))
    {
      glfwPollEvents ();
      _M_draw_frame ();
    }

  vkDeviceWaitIdle (_M_logical_device);
}
