/* © Copyright 2019
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::_M_create_image_views ()
{
  _M_swap_chain_image_views.resize (_M_swap_chain_images.size ());

  for (size_t i = 0; i < _M_swap_chain_images.size (); ++i)
    {
      VkImageViewCreateInfo create_info = {};
      create_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
      create_info.image = _M_swap_chain_images[i];
      create_info.viewType  = VK_IMAGE_VIEW_TYPE_2D;
      create_info.format = _M_swap_chain_image_format;
      create_info.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
      create_info.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
      create_info.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
      create_info.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
      create_info.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
      create_info.subresourceRange.baseMipLevel = 0;
      create_info.subresourceRange.levelCount = 1;
      create_info.subresourceRange.baseArrayLayer = 0;
      create_info.subresourceRange.layerCount = 1;

      if (vkCreateImageView (_M_logical_device, &create_info, nullptr, &_M_swap_chain_image_views[i])
	  != VK_SUCCESS)
	{
	  throw std::runtime_error ("Failed to create image views!");
	}
    }
}
