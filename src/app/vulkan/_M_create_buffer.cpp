/* © Copyright 2019
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::_M_create_buffer (VkDeviceSize size,
			       VkBufferUsageFlags usage,
			       VkMemoryPropertyFlags properties,
			       VkBuffer& buffer,
			       VkDeviceMemory& buffer_memory)
{
  VkBufferCreateInfo buffer_info = {};
  buffer_info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
  buffer_info.size = size;
  buffer_info.usage = usage;
  buffer_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

  if (vkCreateBuffer (_M_logical_device, &buffer_info, nullptr, &buffer) != VK_SUCCESS)
    {
      throw std::runtime_error ("Failed to create buffer!");
    }

  VkMemoryRequirements mem_requirements;
  vkGetBufferMemoryRequirements (_M_logical_device, buffer, &mem_requirements);

  VkMemoryAllocateInfo alloc_info = {};
  alloc_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
  alloc_info.allocationSize = mem_requirements.size;
  alloc_info.memoryTypeIndex = _M_find_memory_type (mem_requirements.memoryTypeBits, properties);

  if (vkAllocateMemory (_M_logical_device, &alloc_info, nullptr, &buffer_memory) != VK_SUCCESS)
    {
      throw std::runtime_error ("Failed to allocate buffer memory!");
    }

  vkBindBufferMemory (_M_logical_device, buffer, buffer_memory, 0);
}
