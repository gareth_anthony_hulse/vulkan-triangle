/* © Copyright 2019
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

VkShaderModule
app::vulkan::_M_create_shader_module (const std::vector<char>& code)
{
  VkShaderModuleCreateInfo create_info = {};
  create_info.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
  create_info.codeSize = code.size ();
  create_info.pCode = reinterpret_cast<const uint32_t*> (code.data ());

  VkShaderModule shader_module;
  if (vkCreateShaderModule (_M_logical_device, &create_info, nullptr, &shader_module) != VK_SUCCESS)
    {
      throw std::runtime_error ("Failed to create shader module.");
    }

  return shader_module;
}
