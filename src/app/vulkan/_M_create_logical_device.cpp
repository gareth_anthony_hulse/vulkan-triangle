/* © Copyright 2019
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::_M_create_logical_device ()
{
  queue_family_indices indices = _M_find_queue_families (_M_physical_device);

  std::vector<VkDeviceQueueCreateInfo> logical_device_queue_create_infos;
  std::set<uint32_t> unique_queue_families =
    {
     indices.graphics_family.value (),
     indices.present_family.value ()
    };

  float queue_priority = 1.0f;
  for (uint32_t queue_family : unique_queue_families)
    {
      VkDeviceQueueCreateInfo logical_device_queue_create_info = {};
      logical_device_queue_create_info.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
      logical_device_queue_create_info.queueFamilyIndex = queue_family;
      logical_device_queue_create_info.queueCount = 1;
      logical_device_queue_create_info.pQueuePriorities = &queue_priority;
      logical_device_queue_create_infos.push_back (logical_device_queue_create_info);
    }

  VkPhysicalDeviceFeatures physical_device_features = {};

  VkDeviceCreateInfo logical_device_create_info = {};
  logical_device_create_info.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;

  logical_device_create_info.queueCreateInfoCount =
    static_cast<uint32_t> (logical_device_queue_create_infos.size ());
  logical_device_create_info.pQueueCreateInfos = logical_device_queue_create_infos.data ();
  
  logical_device_create_info.pEnabledFeatures = &physical_device_features;

  logical_device_create_info.enabledExtensionCount = static_cast<uint32_t>
    (_M_device_extensions.size ());
  logical_device_create_info.ppEnabledExtensionNames = _M_device_extensions.data ();

  if (vkCreateDevice (_M_physical_device, &logical_device_create_info, nullptr, &_M_logical_device)
      != VK_SUCCESS)
    {
      throw std::runtime_error ("Failed to create logical device.");
    }

  vkGetDeviceQueue (_M_logical_device, indices.graphics_family.value (), 0, &_M_graphics_queue);
  vkGetDeviceQueue (_M_logical_device, indices.present_family.value (), 0, &_M_present_queue);
}
