/* © Copyright 2019
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef NDEBUG
#include "../vulkan.hpp"

VkResult
app::vulkan::_M_create_debug_messenger (VkInstance instance,
					const VkDebugUtilsMessengerCreateInfoEXT* create_info,
					const VkAllocationCallbacks* allocator,
					VkDebugUtilsMessengerEXT* debug_messenger)
{
  auto create_debug_utils_messenger_ext =
    reinterpret_cast<PFN_vkCreateDebugUtilsMessengerEXT>
    (vkGetInstanceProcAddr (instance, "vkCreateDebugUtilsMessengerEXT"));
  if (create_debug_utils_messenger_ext != nullptr)
    {
      return create_debug_utils_messenger_ext (instance, create_info, allocator, debug_messenger);
    }

  return VK_ERROR_EXTENSION_NOT_PRESENT;
}
#endif // NDEBUG
