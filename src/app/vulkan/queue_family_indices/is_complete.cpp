/* © Copyright 2019
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: MIT
 */

#include "../../vulkan.hpp"

bool
app::vulkan::queue_family_indices::is_complete ()
{
  return graphics_family.has_value () && present_family.has_value ();
}
