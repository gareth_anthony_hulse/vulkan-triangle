/* © Copyright 2019
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::_M_cleanup_swap_chain ()
{
  for (auto framebuffer : _M_swap_chain_framebuffers)
    {
      vkDestroyFramebuffer (_M_logical_device, framebuffer, nullptr);
    }
  vkDestroyPipeline (_M_logical_device, _M_graphics_pipeline, nullptr);
  vkDestroyPipelineLayout (_M_logical_device, _M_pipeline_layout, nullptr);
  vkDestroyRenderPass (_M_logical_device, _M_render_pass, nullptr);
  for (auto image_view: _M_swap_chain_image_views)
    {
      vkDestroyImageView (_M_logical_device, image_view, nullptr);
    }
  vkDestroySwapchainKHR (_M_logical_device, _M_swap_chain, nullptr);
}
