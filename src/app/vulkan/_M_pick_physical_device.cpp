/* © Copyright 2019
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::_M_pick_physical_device ()
{
  uint32_t physical_device_count = 0;
  vkEnumeratePhysicalDevices (_M_instance, &physical_device_count, nullptr);
  if (physical_device_count == 0)
    {
      throw std::runtime_error ("Failed to find a physical device with Vulkan Support.");
    }

  std::vector<VkPhysicalDevice> physical_devices (physical_device_count);
  vkEnumeratePhysicalDevices (_M_instance, &physical_device_count, physical_devices.data ());

  for (const auto& physical_device : physical_devices)
    {
      if (_M_is_physical_device_suitable (physical_device))
	{
	  _M_physical_device = physical_device;
	  break;
	}
    }

  if (_M_physical_device == VK_NULL_HANDLE)
    {
      throw std::runtime_error ("Failed to find suitable physical device.");
    }
}
