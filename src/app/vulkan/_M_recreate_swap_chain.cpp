/* © Copyright 2019
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "../vulkan.hpp"

void
app::vulkan::_M_recreate_swap_chain ()
{
  vkDeviceWaitIdle (_M_logical_device);

  _M_cleanup_swap_chain ();

  _M_create_swap_chain ();
  _M_create_image_views ();
  _M_create_render_pass ();
  _M_create_framebuffers ();
  _M_create_command_buffers ();
}
