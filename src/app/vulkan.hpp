/* © Copyright 2019
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef APP_VULKAN_HPP
#define APP_VULKAN_HPP

#define GLFW_INCLUDE_NONE
#include <vulkan/vulkan.h>
#include <GLFW/glfw3.h>
#include <cstdio>
#include <stdexcept>
#include <functional>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <set>
#include <cstring>
#include <optional>
#include <cstdint>
#include <fstream>
#include <glm/glm.hpp>
#include <array>

namespace app
{
  class vulkan
  {
  public:
    // Functions:
    ~vulkan ();
    
    void run ();

  private:
    // Structs:
    struct queue_family_indices
    {
      std::optional<uint32_t> graphics_family;
      std::optional<uint32_t> present_family;

      bool is_complete ();
    };

    struct swap_chain_support_details
    {
      VkSurfaceCapabilitiesKHR capabilities;
      std::vector<VkSurfaceFormatKHR> formats;
      std::vector<VkPresentModeKHR> present_modes;
    };

    struct vertex
    {
      glm::vec2 pos;
      glm::vec3 color;

      static VkVertexInputBindingDescription get_binding_description ();
      static std::array<VkVertexInputAttributeDescription, 2> get_attribute_descriptions ();
    };
    
    // Variables:
#ifndef NDEBUG
    const std::vector<const char*> _M_validation_layers =
      {
       "VK_LAYER_KHRONOS_validation"
      };
    VkDebugUtilsMessengerEXT _M_debug_messenger;
#endif // NDEBUG
    
    const std::vector<const char*> _M_device_extensions =
      {
       VK_KHR_SWAPCHAIN_EXTENSION_NAME
      };
    const std::vector<app::vulkan::vertex> _M_vertices =
      {
       /* {{x, y},
	*  {red, green, blue}}
	*/
       
       {{0.0f, -0.5f},
	{1.0f, 0.0f, 0.0f}},
       
       {{0.375f, 0.5f},
	{0.0f, 1.0f, 0.0f}},
       
       {{-0.375f, 0.5f},
	{0.0f, 0.0f, 1.0f}}
      };
    
    GLFWwindow* _M_window;
    VkInstance _M_instance;
    VkPhysicalDevice _M_physical_device = VK_NULL_HANDLE;
    VkDevice _M_logical_device;
    VkQueue _M_graphics_queue;
    VkQueue _M_present_queue;
    VkSurfaceKHR _M_surface;
    const std::string _M_target_session_type = "wayland";
    VkSwapchainKHR _M_swap_chain;
    std::vector<VkImage> _M_swap_chain_images;
    VkFormat _M_swap_chain_image_format;
    VkExtent2D _M_swap_chain_extent;
    std::vector<VkImageView> _M_swap_chain_image_views;
    VkRenderPass _M_render_pass;
    VkPipelineLayout _M_pipeline_layout;
    VkPipeline _M_graphics_pipeline;
    std::vector<VkFramebuffer> _M_swap_chain_framebuffers;
    VkCommandPool _M_command_pool;
    std::vector<VkCommandBuffer> _M_command_buffers;
    std::vector<VkSemaphore> _M_image_available_semaphores;
    std::vector<VkSemaphore> _M_render_finished_semaphores;
    std::vector<VkFence> _M_in_flight_fences;
    std::vector<VkFence> _M_images_in_flight;
    const uint _M_max_frames_in_flight = 2;
    size_t _M_current_frame = 0;
    bool _M_framebuffer_resized = false;
    VkBuffer _M_vertex_buffer;
    VkDeviceMemory _M_vertex_buffer_memory;
    
    // Functions:
#ifndef NDEBUG
    bool _M_check_validation_layer_support ();
    static VKAPI_ATTR VkBool32 VKAPI_CALL
    _M_debug_callback (VkDebugUtilsMessageSeverityFlagBitsEXT message_severity,
		       VkDebugUtilsMessageTypeFlagsEXT message_type,
		       const VkDebugUtilsMessengerCallbackDataEXT* callback_data,
		       void* user_data);
    void _M_setup_debug_messenger ();
    VkResult _M_create_debug_messenger (VkInstance instance,
					const VkDebugUtilsMessengerCreateInfoEXT* create_info,
					const VkAllocationCallbacks* allocator,
					VkDebugUtilsMessengerEXT* debug_messenger);
    void _M_destroy_debug_messenger (VkInstance instance,
				     VkDebugUtilsMessengerEXT debug_messenger,
				     const VkAllocationCallbacks* allocator);
    void _M_populate_debug_messenger_create_info (VkDebugUtilsMessengerCreateInfoEXT& create_info);
#endif //NDEBUG
    void _M_init_window ();
    void _M_init_vulkan ();
    void _M_main_loop ();

    void _M_create_instance ();
    std::vector<const char*> _M_get_required_instance_exts ();
    void _M_pick_physical_device ();
    bool _M_is_physical_device_suitable (VkPhysicalDevice physical_device);
    queue_family_indices _M_find_queue_families (VkPhysicalDevice physical_device);
    void _M_create_logical_device ();
    void _M_create_surface ();
    bool _M_check_device_extension_support (VkPhysicalDevice physical_device);
    swap_chain_support_details _M_query_swap_chain_support (VkPhysicalDevice physical_device);
    VkSurfaceFormatKHR _M_choose_swap_surface_format (const std::vector<VkSurfaceFormatKHR>&
						      available_formats);
    VkPresentModeKHR _M_choose_swap_present_mode (const std::vector<VkPresentModeKHR>&
						  available_present_modes);
    VkExtent2D _M_choose_swap_extent (const VkSurfaceCapabilitiesKHR& capabilities);
    void _M_create_swap_chain ();
    void _M_create_image_views ();
    void _M_create_graphics_pipeline ();
    static std::vector<char> _M_read_file (const std::string& filename);
    VkShaderModule _M_create_shader_module (const std::vector<char>& code);
    void _M_create_render_pass ();
    void _M_create_framebuffers ();
    void _M_create_command_pool ();
    void _M_create_command_buffers ();
    void _M_draw_frame ();
    void _M_create_sync_objects ();
    void _M_recreate_swap_chain ();
    void _M_cleanup_swap_chain ();
    static void _M_framebuffer_resize_callback (GLFWwindow* window, int width, int height);
    void _M_create_vertex_buffer ();
    uint32_t _M_find_memory_type (uint32_t type_filter, VkMemoryPropertyFlags properties);
    void _M_create_buffer (VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties,
			   VkBuffer& buffer, VkDeviceMemory& buffer_memory);
    void _M_copy_buffer (VkBuffer src_buffer, VkBuffer dst_buffer, VkDeviceSize size);
  };
  
} // namespace app
#endif // APP_VULKAN_HPP
